---
- name: Configure Fedora workstation
  hosts: localhost
  connection: local

  vars_files:
    - default.config.yml

  handlers:
    - name: Clean yum metadata
      ansible.builtin.command:  # noqa no-changed-when
        cmd: yum clean metadata
      become: true

  tasks:
    - name: Setup Ansible
      ansible.builtin.import_tasks: tasks/setup-ansible.yml
      tags: always

    - name: Ensure downloads directory
      ansible.builtin.file:
        path: "{{ downloads }}"
        state: directory
        mode: '0755'
      tags: always

    - name: Configure dotfiles
      ansible.builtin.import_tasks: tasks/dotfiles.yml
      when: configure_dotfiles
      tags: dotfiles

    - name: Configure Fedora
      ansible.builtin.import_tasks: tasks/fedora.yml
      when: configure_fedora
      tags: workstation

    - name: Configure GNOME
      ansible.builtin.import_tasks: tasks/gnome.yml
      when: configure_gnome
      tags: workstation

    - name: Configure Developer Tools
      ansible.builtin.import_tasks: tasks/devtools.yml
      when: configure_devtools
      tags: devtools

    - name: Configure Radio Apps
      ansible.builtin.import_tasks: tasks/radio.yml
      when: configure_radio
      tags: radio
