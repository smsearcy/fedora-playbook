Fedora Playbook
===============

[Ansible](https://www.ansible.com/) playbook to setup my Fedora workstation for
development and amateur radio (currently running Fedora 36).

This playbook was reorganized in May 2020 based on
[geerlingguy/mac-dev-playbook](https://github.com/geerlingguy/mac-dev-playbook).


Running
-------

To run this playbook:

1. Install Ansible and Git: `$ sudo dnf install -y ansible-core ansible-collection-ansible-posix ansible-collection-community-general git`
2. Install Homebrew: `/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`
3. Clone this repository and `cd` to that directory
4. Run `ansible-playbook playbook.yml -K` (enter your password for `sudo` when prompted)


Testing
-------

A couple of rudimentary tests are available:

* `ansible-lint` can be executed via `just pre-commit` (it is also ran via GitLab CI).
