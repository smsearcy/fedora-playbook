default: pre-commit clean

# Run pre-commit
pre-commit:
    pre-commit run --all-files

# Delete retry files
clean:
    find . -name "*.retry" -delete

# Run the playbook
run:
    ansible-playbook playbook.yml --ask-become-pass
